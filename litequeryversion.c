#include <stdlib.h>
#include <stdio.h>
#include <sqlite3.h>

int main(void)
{
	sqlite3      *db;
	sqlite3_stmt *res;

	int con = sqlite3_open(":memory:", &db);
	if (con != SQLITE_OK) {
		fprintf(stdout, "Can't open database. %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
		return EXIT_FAILURE;
	}

	con = sqlite3_prepare(db, "SELECT SQLITE_VERSION()", -1, &res, 0);
	if (con != SQLITE_OK) {
		fprintf(stderr, "Failed to execute command: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
		EXIT_FAILURE;
	}

	con = sqlite3_step(res);
	if (con == SQLITE_ROW) {
		fprintf(stdout, "Sqlite3 Version is: %s\n", sqlite3_column_text(res, 0));
	}

	sqlite3_finalize(res);
	sqlite3_close(db);

	return EXIT_SUCCESS;
}